# FLIGHT BOOKER README

This project is part of The Odin Project Ruby on Rails course. 

[Project Link](https://www.theodinproject.com/courses/ruby-on-rails/lessons/building-advanced-forms "Building Advanced Forms")

The purpose is to create a flight booking website in order to learn about creating advanced forms with multiple models.