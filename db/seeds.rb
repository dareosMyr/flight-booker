# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Create Airports
Airport.create!([
{ name: 'San Francisco International', code: 'SFO' },
{ name: 'John F. Kennedy International', code: 'JFK'},
{ name: 'Los Angeles International', code: 'LAX' },
{ name: 'Chicago O\'Hare International', code: 'ORD'},
{ name: 'Hartsfield-Jackson Atlanta International', code: 'ATL'},
{ name: 'Dallas/Fort Worth International', code: 'DFW'},
{ name: 'Denver International', code: 'DEN'}
])

#Create flights
@time = Time.now
def gen_flights
  start = rand(1..7)
  array = [1,2,3,4,5,6,7]
  array - [start]
  stop = array.sample
  Flight.create!(start: start, end: stop, date: @time + rand(1..5).days, duration: rand(1..6).hours)
end

300.times do 
  gen_flights
end

=begin
time = Time.now
Flight.create!([
  {start: 1, end: 2, date: time + rand(1...13).days, duration: 5.hours},
  {start: 3, end: 2, date: time + rand(1...13).days, duration: 5.5.hours},
  {start: 2, end: 4, date: time + rand(1...13).days, duration: 3.hours},
  {start: 5, end: 6, date: time + rand(1...13).days, duration: 4.hours},
  {start: 6, end: 5, date: time + rand(1...13).days, duration: 2.5.hours},
  {start: 7, end: 1, date: time + rand(1...13).days, duration: 2.4.hours},
  {start: 5, end: 7, date: time + rand(1...13).days, duration: 1.3.hours},
  {start: 7, end: 2, date: time + rand(1...13).days, duration: 4.3.hours},
  {start: 3, end: 5, date: time + rand(1...13).days, duration: 3.hours},
  {start: 4, end: 1, date: time + rand(1...13).days, duration: 2.3.hours},
  {start: 1, end: 4, date: time + rand(1...13).days, duration: 5.4.hours}
])
=end

=begin
time = Time.now
300.times do 
  Flight.create!(start: rand(1...7), end: rand(1...7), date: time + rand(1...13).days, duration: rand(1...6).hours)
end
=end
