class CreateFlights < ActiveRecord::Migration[6.0]
  def change
    create_table :flights do |t|
      t.integer :start
      t.integer :end
      t.datetime :date
      t.string :duration

      t.timestamps
    end
  end
end
