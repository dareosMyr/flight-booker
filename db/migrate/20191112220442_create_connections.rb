class CreateConnections < ActiveRecord::Migration[6.0]
  def change
    create_table :connections, id: false do |t|
      t.integer :flight_id
      t.integer :airport_id

      t.timestamps
    end
  end
end
