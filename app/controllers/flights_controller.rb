class FlightsController < ApplicationController
  def index
    @airports = Airport.all.map { |f| ["#{f.name} (#{f.code})", f.id] }
    @dates_distinct = Flight.select(:date).distinct.order('date ASC')
    @dates = @dates_distinct.all.map { |f| [f.date.strftime('%m/%d/%y'), f.date] }
    if params[:search]
      @flights = Flight.where(
        start: params[:search][:start], 
        end: params[:search][:end], 
        date: date_range(params[:search][:date])
        ).limit(30)
    end

  end

  def fill(param)
    if params[:search]
      params[:search][param]
    else
      params[:search]
    end
  end

  def date_range(date)
    Date.parse("#{date}").beginning_of_day..Date.parse("#{date}").end_of_day
  end

  private

end
