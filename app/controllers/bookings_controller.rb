class BookingsController < ApplicationController
  def new
    @flight = Flight.find_by(id: params[:flight])
    @passenger_num = params[:passengers]
    @booking = Booking.new
    @passenger_num.to_i.times { @booking.passengers.build }
  end

  def create
    @booking = Booking.new(booking_params)
    if @booking.save
      redirect_to @booking
      @booking.passengers.each do |p|
        PassengerMailer.with(user: p).thanks_email.deliver_later
      end
    else
      render 'new'
    end

  end

  def show
    @booking = Booking.find_by(id: params[:id])
  end

  private

    def booking_params
      params.require(:booking).permit(:flight_id, passengers_attributes: [:name, :email])
    end

end
