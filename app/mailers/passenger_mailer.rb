class PassengerMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def thanks_email
    @passenger = params[:user]
    mail(to: @passenger.email, subject: 'Your booking has been confirmed.')
  end
  
end
