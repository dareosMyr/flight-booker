class Flight < ApplicationRecord
  has_one :departure, class_name: 'Airport', primary_key: :start, foreign_key: :id
  has_one :arrival, class_name: 'Airport', primary_key: :end, foreign_key: :id
  has_many :bookings
end
