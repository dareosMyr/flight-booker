class Booking < ApplicationRecord
  has_many :passengers
  has_one :flight, foreign_key: :id, primary_key: :flight_id
  accepts_nested_attributes_for :passengers
end
